import com.mongodb.DBCollection;
import com.mongodb.MongoClient;

import java.net.UnknownHostException;

public class MongoDriverJavaHelpers {

    public static DBCollection getCollection(String dbName, String cName) throws UnknownHostException {

        return new MongoClient()
                .getDB(dbName)
                .getCollection(cName);
    }
}
